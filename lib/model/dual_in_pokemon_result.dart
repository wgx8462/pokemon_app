import 'package:pokemon_app/model/dual_in_pokemons.dart';

class DualInPokemonResult {
  DualInPokemons data;

  DualInPokemonResult(this.data);

  factory DualInPokemonResult.fromJson(Map<String, dynamic> json) {
    return DualInPokemonResult(
        DualInPokemons.fromJson(json['data'])
    );
  }
}