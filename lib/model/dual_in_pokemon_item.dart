class DualInPokemonItem {
  num pokemonDualId;
  num pokemonId;
  String imageUrl;
  String name;
  num vitality;
  num power;
  num defensive;
  num speed;

  DualInPokemonItem(
      this.pokemonDualId,
      this.pokemonId,
      this.imageUrl,
      this.name,
      this.vitality,
      this.power,
      this.defensive,
      this.speed,
      );

  factory DualInPokemonItem.fromJson(Map<String, dynamic> json) {
    return DualInPokemonItem(
        json['pokemonDualId'],
        json['pokemonId'],
        json['imageUrl'],
        json['name'],
        json['vitality'],
        json['power'],
        json['defensive'],
        json['speed'],
    );
  }
}