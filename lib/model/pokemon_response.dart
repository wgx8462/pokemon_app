class PokemonResponse {
  num id;
  String imageUrl;
  String name;
  String type;
  String characteristic;
  String classification;
  String explanation;
  num vitality;
  num power;
  num defensive;
  num speed;

  PokemonResponse(this.id, this.imageUrl, this.name, this.type, this.characteristic, this.classification, this.explanation, this.vitality, this.power, this.defensive, this.speed);

  factory PokemonResponse.fromJson(Map<String, dynamic> json) {
    return PokemonResponse(
        json['id'],
        json['imageUrl'],
        json['name'],
        json['type'],
        json['characteristic'],
        json['classification'],
        json['explanation'],
        json['vitality'],
        json['power'],
        json['defensive'],
        json['speed'],
    );
  }
}