import 'dart:math';

import 'package:pokemon_app/model/dual_in_pokemon_item.dart';

class DualInPokemons {
  final DualInPokemonItem ? pokemon1;
  final DualInPokemonItem ? pokemon2;

  const DualInPokemons(this.pokemon1, this.pokemon2);

  factory DualInPokemons.fromJson(Map<String, dynamic> json) {
    return DualInPokemons(
      json['pokemon1'] != null ? DualInPokemonItem.fromJson(json['pokemon1']) : null,
      json['pokemon2'] != null ? DualInPokemonItem.fromJson(json['pokemon2']) : null
    );
  }
}