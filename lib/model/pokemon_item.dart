class PokemonItem {
  num id;
  String imageUrl;
  String name;
  String type;

  PokemonItem(this.id, this.imageUrl, this.name, this.type);

  factory PokemonItem.fromJson(Map<String, dynamic> json) {
    return PokemonItem(
        json['id'],
        json['imageUrl'],
        json['name'],
        json['type'],
    );
  }
}