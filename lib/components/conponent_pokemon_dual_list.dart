import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:pokemon_app/model/dual_in_pokemon_item.dart';

class ComponentPokemonDualList extends StatefulWidget {
  const ComponentPokemonDualList({
    super.key,
    required this.pokemonItem,
    required this.callback,
    required this.isMyTurn,
    required this.isLive,
    required this.currentHp,
  });

  final DualInPokemonItem pokemonItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentPokemonDualList> createState() =>
      _ComponentPokemonDualListState();
}

class _ComponentPokemonDualListState extends State<ComponentPokemonDualList> {
  double _calculateHpPercent() {
    return widget.currentHp / widget.pokemonItem.vitality;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 4,
            height: MediaQuery.of(context).size.height / 4,
            child: Image.asset(
              '${widget.isLive ? widget.pokemonItem.imageUrl : 'assets/mom07.jpg'}', // 살아있으면 기존거로 바이지만 죽으면 다른이미지로 바꾼다.
              fit: BoxFit.contain,
            ),
          ),

          Text(
              widget.pokemonItem.name,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text('HP : '),
              SizedBox(
                width: 25,
                child: Text(
                  '${widget.currentHp}',
                  style: TextStyle(
                    color: _calculateHpPercent() < 0.4 ? Colors.redAccent : Colors.green,
                  ),
                ),
              ),
              SizedBox(width: 22,),
              Text('공격력 : ${widget.pokemonItem.power}'),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('방어력 : ${widget.pokemonItem.defensive}'),
              SizedBox(width: 10,),
              Text('스피드 : ${widget.pokemonItem.speed}'),
            ],
          ),
          SizedBox(
            height: 7,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width / 3,
            child: LinearPercentIndicator(
              percent: _calculateHpPercent(),
              lineHeight: 7,
              animation: true,
              animateFromLastPercent: true,
              animationDuration: 700,
              progressColor: _calculateHpPercent() < 0.4 ? Colors.redAccent : Colors.green,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          (widget.isMyTurn && widget.isLive) // 내턴이 아닐경우 공격 버튼이 안보이게 대체
              ? ElevatedButton(
                  onPressed: widget.callback,
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.lightBlueAccent,
                    foregroundColor: Colors.white,
                  ),
                  child: const Text('공격'),
                )
              : Container(
            margin: EdgeInsets.fromLTRB(0, 32, 0, 0),
            child: Column(),
          ),
        ],
      ),
    );
  }
}
