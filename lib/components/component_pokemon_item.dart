import 'package:flutter/material.dart';
import 'package:pokemon_app/model/pokemon_item.dart';

class ComponentPokemonItem extends StatelessWidget {
  const ComponentPokemonItem({
    super.key,
    required this.pokemonItem,
    required this.callback,
  });

  final PokemonItem pokemonItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          SizedBox(
            width: 250,
            height: 200,
            child: Image.asset('assets/${pokemonItem.imageUrl}'),
          ),
          Text(
              pokemonItem.name,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text('타입 : ${pokemonItem.type}'),
        ],
      ),
    );
  }
}
