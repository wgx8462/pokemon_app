import 'package:dio/dio.dart';
import 'package:pokemon_app/config/config_api.dart';
import 'package:pokemon_app/model/pokemon_detail_result.dart';
import 'package:pokemon_app/model/pokemon_list_result.dart';

class RepoPokemon {

  Future<PokemonListResult> getPokemons() async {
    Dio dio = Dio();

    final String _baseUil = '$apiUri/pokemon/all';

    final response = await dio.get(_baseUil,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return PokemonListResult.fromJson(response.data);
  }

  Future<PokemonDetailResult> getPokemon(num id) async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/pokemon/detail/{id}';

    final response = await dio.get(_baseUrl.replaceAll('{id}', id.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return PokemonDetailResult.fromJson(response.data);
  }
}