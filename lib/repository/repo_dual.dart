import 'package:dio/dio.dart';
import 'package:pokemon_app/config/config_api.dart';
import 'package:pokemon_app/model/common_result.dart';
import 'package:pokemon_app/model/dual_in_pokemon_result.dart';

class RepoDual {
  Future<DualInPokemonResult> getCurrentState() async {
    Dio dio = Dio();

    final String _baseUil = '$apiUri/dual/dual-stage/current';

    final response = await dio.get(_baseUil, //http 메서드 멋있는 이름
        options: Options(
        followRedirects: false, // 다른데로 따라갈거야 false 안따라갈래
        validateStatus: (status) {
          return status == 200;
        }
    )
    );
    return DualInPokemonResult.fromJson(response.data);
  }

  Future<CommonResult> setStageIn(num pokemonId) async {
    Dio dio = Dio();

    final String _baseUil = '$apiUri/dual/pokemon-id/{pokemonId}/join';

    final response = await dio.post(_baseUil.replaceAll('{pokemonId}', pokemonId.toString()),
        options: Options(
        followRedirects: false, // 다른데로 따라갈거야 false 안따라갈래
        validateStatus: (status) {
          return status == 200;
        }
    )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> delStagePokemon(num dualId) async {
    Dio dio = Dio();

    final String _baseUil = '$apiUri/dual/dual-id/{dualId}';

    final response = await dio.delete(_baseUil.replaceAll('{dualId}', dualId.toString()),
        options: Options(
            followRedirects: false, // 다른데로 따라갈거야 false 안따라갈래
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}