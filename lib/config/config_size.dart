const double fontSizeSuper = 24;
const double fontSizeBig = 18;
const double fontSizeMid = 16;
const double fontSizeSm = 14;
const double fontSizeMicro = 12;

const double appbarSizeHeight = 50;