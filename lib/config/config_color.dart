import 'package:flutter/material.dart';

const Color colorPrimary = Color.fromRGBO(255, 0, 0, 1);
const Color colorSecondary = Color.fromRGBO(0, 255, 0, 1);