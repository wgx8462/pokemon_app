import 'package:flutter/material.dart';
import 'package:pokemon_app/components/component_pokemon_item.dart';
import 'package:pokemon_app/model/pokemon_item.dart';
import 'package:pokemon_app/pages/page_pokemon_detail.dart';
import 'package:pokemon_app/repository/repo_pokemon.dart';

class PagePokemon extends StatefulWidget {
  const PagePokemon({super.key});

  @override
  State<PagePokemon> createState() => _PagePokemonState();
}

class _PagePokemonState extends State<PagePokemon> {
  List<PokemonItem> _list = [];

  Future<void> _loadList() async {
    await RepoPokemon()
        .getPokemons()
        .then((res) => {
              setState(() {
                _list = res.list;
              })
            });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset('assets/logo.png'),
      ),
      drawer: Drawer(),
      body: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: GridView.builder(
          shrinkWrap: true,
            itemCount: _list.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 1 / 2,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
            ),
            itemBuilder: (BuildContext context, int idx) {
            return ComponentPokemonItem(pokemonItem: _list[idx], callback: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PagePokemonDetail(id: _list[idx].id)));
            });
            }),
      )
    );
  }
}
