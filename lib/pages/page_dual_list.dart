import 'package:flutter/material.dart';
import 'package:pokemon_app/model/dual_in_pokemon_item.dart';
import 'package:pokemon_app/pages/page_dual.dart';
import 'package:pokemon_app/pages/page_pokemon.dart';
import 'package:pokemon_app/repository/repo_dual.dart';

class PageDualList extends StatefulWidget {
  const PageDualList({super.key});

  @override
  State<PageDualList> createState() => _PageDualListState();
}

class _PageDualListState extends State<PageDualList> {
  DualInPokemonItem? pokemon1;
  DualInPokemonItem? pokemon2;

  // 현재 결투장에 진입중인 몬스터의 정보를 가져오기
  Future<void> _loadDetail() async {
    await RepoDual().getCurrentState()
        .then((res) =>
    {
      setState(() {
        pokemon1 = res.data.pokemon1;
        pokemon2 = res.data.pokemon2;
      })
    });
  }

  // 결투장에서 몬스터 퇴출시키기
  Future<void> _delPokemon(num dualId) async {
    await RepoDual().delStagePokemon(dualId)
        .then((res) =>
    {
      _loadDetail() // 퇴출에 성공하면 현재 결투장에 진입중인 몬스터 정보 새로 가져와서 다시 갱신
    });
  }

  // 페이지 생성되고 화면에 부착하기전에 데이터부터 쥐어줘야함.
  @override
  void initState() {
    super.initState();
    setState(() {
      _loadDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset('assets/logo.png'),
      ),
      drawer: Drawer(),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery
        .of(context)
        .size
        .width;

    return SingleChildScrollView(
      child: Column(
        children: [
          pokemon1 != null ?
          Container(
            alignment: Alignment.topRight,
            margin: EdgeInsets.fromLTRB(0, 0, 30, 0),
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    _delPokemon(pokemon1!.pokemonDualId);
                  },
                  child: Text('퇴장'),
                ),
                Image.asset(
                  'assets/${pokemon1!.imageUrl}',
                  width: phoneWidth / 3,
                  height: phoneWidth / 3,
                  fit: BoxFit.contain,
                ),
              ],
            ),
          )
              : Container(),

          Container(
            child: const Column(
              children: [
                Text(
                  'VS',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),

          (pokemon2 != null) ?
          Container(
            alignment: Alignment.bottomLeft,
            margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    _delPokemon(pokemon2!.pokemonDualId);
                  },
                  child: Text('퇴장'),
                ),
                Image.asset(
                  'assets/${pokemon2!.imageUrl}',
                  width: phoneWidth / 3,
                  height: phoneWidth / 3,
                  fit: BoxFit.contain,
                ),
              ],
            ),
          )
              : Container(),

          (pokemon1 != null && pokemon2 != null) ?
          Column(
            children: [
              Container(
                alignment: Alignment.bottomRight,
                margin: EdgeInsets.fromLTRB(0, 0, 70, 0),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            PageDual(
                                pokemon1: pokemon1!, pokemon2: pokemon2!)));
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.lightBlueAccent,
                    foregroundColor: Colors.white,
                  ),
                  child: const Text(
                    '배틀 입장',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Container(
                child: OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => PagePokemon()));
                  },
                  child: Text('나가기'),
                ),
              ),
            ],
          )
              : Container(
              child: OutlinedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PagePokemon()));
                },
                child: Text('포켓몬 보기'),
              ),
          ),
        ],
      ),
    );
  }
}
