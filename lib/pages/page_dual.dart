import 'package:flutter/material.dart';
import 'package:pokemon_app/components/conponent_pokemon_dual_list.dart';
import 'package:pokemon_app/model/dual_in_pokemon_item.dart';
import 'dart:math';

import 'package:pokemon_app/pages/page_pokemon.dart';

class PageDual extends StatefulWidget {
  const PageDual({
    super.key,
    required this.pokemon1,
    required this.pokemon2,
  });

  // 생성자를 통해서 몬스터1이랑 몬스터2 정보 받아옴.
  final DualInPokemonItem pokemon1;
  final DualInPokemonItem pokemon2;

  get randomBool => Random().nextBool();
  @override
  State<PageDual> createState() => _PageDualState();
}


class _PageDualState extends State<PageDual> {

  bool _isTurnPokemon1 = true; // 기본으로 1번 포켓몬 선빵, 1번 포켓몬 순서면 2번 포켓몬 순서가 아님

  bool _pokemon1Live = true;
  num _pokemon1CurrentHp = 0;

  bool _pokemon2Live = true;
  num _pokemon2CurrentHp = 0;

  String _gameLog = '';

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn();
    _pokemon1CurrentHp = widget.pokemon1.vitality;  // 1번 포켓몬 잔여 hp (페이지 들어가자마자 니까 최대체력만큼)
    _pokemon2CurrentHp = widget.pokemon2.vitality;  // 2번 포켓몬 잔여 hp (페이지 들어가자마자 니까 최대체력만큼)
  }

  // 선빵 누구인지 계산
  void _calculateFirstTurn() {
    if (widget.pokemon1.speed < widget.pokemon2.speed) { // 포켓몬1번보다 포켓몬2번이 스피드가 높으면
      setState(() {
        _isTurnPokemon1 = false; // 포켓몬 2번이 선빵을 한다.
      });
    } else if (widget.pokemon1.speed == widget.pokemon2.speed) {
      setState(() {
        _isTurnPokemon1 = widget.randomBool;
      });
    }
  }

  // 때렸을때 최종 공격력 몇인지 (크리티컬 계산식 넣기)
  num _calculateResultHitPoint(num myHitPower, num targetDefensive) {
    List<num> criticalArr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]; //확률 배열 10개 하나당 10퍼센트
    criticalArr.shuffle(); // 셔플로 순서를 섞는다.

    bool isCritical = false; // 기본적으로는 크리티컬이 안터진다.
    if (criticalArr[0] <= 7) isCritical = true; // 7보다 작거나 같으면 트루, 80% 센트 확률로 크리티컬이 터진다.

    num resultHit = myHitPower; // 리절트히트 = 마이히트파워
    if (isCritical) resultHit = resultHit * 3; // 크리티컬 데미지 3배
    resultHit = resultHit - targetDefensive; // 리절트히트는 리절트히트 - 타겟방어력
    resultHit = resultHit.round(); // 반올림 처리

    if (resultHit <= 0) resultHit = 1; // 계산 결과 공격이 0이거나 음수면 적의 체력을 체워줄수는 없으므로 고정1 처리

    return resultHit;
  }

  // 상대가 포켓몬이 죽었는지 확인하기
  void _checkIsDead(num targetPokemon) {
    if (targetPokemon == 1 && (_pokemon1CurrentHp <= 0)) { // 논리 연산자 &&은 a 와 b 모두 true 일 경우 true이다. 타겟 포켓몬이 1이고 체력이 0이하일 경우 포켓몬1의 값을 false로 바꾼다.
      setState(() {
        _pokemon1Live = false;
      });
    } else if (targetPokemon == 2 && (_pokemon2CurrentHp <= 0)) { //위와 마찬가지로 타겟 포켓몬이2이고 체력이 0이하일 경우 포켓몬2의 값을 false로 바꾼다.
      setState(() {
        _pokemon2Live = false;
      });
    }
  }

  //공격 처리
  void _attPokemon(num actionPokemon) {
    num myHitPower = widget.pokemon1.power;
    num targetDefensive = widget.pokemon2.defensive;

    if (actionPokemon == 2) {
      myHitPower = widget.pokemon2.power;
      targetDefensive = widget.pokemon2.defensive;
    }

    num resultHit = _calculateResultHitPoint(myHitPower, targetDefensive);

    if (actionPokemon == 1) { // 공격하는 포켓몬이 1번이면 (공격 당하는 포켓몬이 2번)
      setState(() {
        _pokemon2CurrentHp -= resultHit; // 2번 포켓몬 체력 깎기
        if (_pokemon2CurrentHp <= 0) _pokemon2CurrentHp = 0; //체력 0 미만이면 0 고정
        _checkIsDead(2); // 2번 포켓몬 죽었는지 확인
      });
    } else {
      setState(() {
        _pokemon1CurrentHp -= resultHit; // 1번 포켓몬 체력 깎기
        if (_pokemon1CurrentHp <= 0) _pokemon1CurrentHp = 0; // 체력이 0 미만이면 0 고정
        _checkIsDead(1); // 1번 포켓몬 죽었는지 확인
      });
    }

    setState(() {
      _isTurnPokemon1 = !_isTurnPokemon1; // 턴 넘기기. not 연산자로 계속 바꿔주기
    });
  }


    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('듀얼'),
      ),
      drawer: Drawer(),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            alignment: Alignment.topRight,
            margin: EdgeInsets.fromLTRB(0, 0, 30, 0),
            child: ComponentPokemonDualList(
              pokemonItem: widget.pokemon1,
              callback: () {
                _attPokemon(1);
              },
              isMyTurn: _isTurnPokemon1,
              isLive: _pokemon1Live,
              currentHp: _pokemon1CurrentHp,
            ),
          ),
          Container(
            alignment: Alignment.bottomLeft,
            margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
            child: ComponentPokemonDualList(
              pokemonItem: widget.pokemon2,
              callback: () {
                _attPokemon(2);
              },
              isMyTurn: !_isTurnPokemon1,
              isLive: _pokemon2Live,
              currentHp: _pokemon2CurrentHp,
            ),
          ),
          (_pokemon1Live != false && _pokemon2Live != false ) ? Container() :
          OutlinedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PagePokemon()));
              },
              child: Text('나가기'),
          ),
          Text(_gameLog),
        ],
      ),
    );
  }
}
