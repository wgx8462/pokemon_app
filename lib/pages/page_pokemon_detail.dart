import 'package:flutter/material.dart';
import 'package:pokemon_app/model/pokemon_response.dart';
import 'package:pokemon_app/pages/page_dual_list.dart';
import 'package:pokemon_app/repository/repo_dual.dart';
import 'package:pokemon_app/repository/repo_pokemon.dart';

class PagePokemonDetail extends StatefulWidget {
  const PagePokemonDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PagePokemonDetail> createState() => _PagePokemonDetailState();
}

class _PagePokemonDetailState extends State<PagePokemonDetail> {
  PokemonResponse? _detail;

  @override
  void initState() {
    setState(() {
      super.initState();
      _loadDetail();
    });
  }

  Future<void> _loadDetail() async {
    await RepoPokemon().getPokemon(widget.id).then((res) => {
          setState(() {
            _detail = res.data;
          })
        });
  }

  Future<void> _setPokemon() async {
    await RepoDual().setStageIn(widget.id).then((res) => {
      setState(() {
        _loadDetail();
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상세보기'),
      ),
      body: buildBody(context),
    );
  }

  Widget buildBody(BuildContext context) {
    if (_detail == null) {
      // 이게 있어야 null 오류가 안뜸
      return const Text('데이터 로딩중..');
    } else {
      return SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Text('${_detail!.id}'),
              Image.asset(
                'assets/${_detail!.imageUrl}',
                width: 300,
                height: 300,
                fit: BoxFit.contain,
              ),
              Text(_detail!.name),
              Text(_detail!.type),
              Text(_detail!.characteristic),
              Text(_detail!.classification),
              Text(_detail!.explanation),
              Text('Hp : ${_detail!.vitality}'),
              Text('공격력 : ${_detail!.power}'),
              Text('방어력 : ${_detail!.defensive}'),
              Text('스피드 : ${_detail!.speed}'),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OutlinedButton(onPressed: () {
                    _setPokemon();
                  }, child: Text('포켓몬 등록')),
                  SizedBox(
                    width: 20,
                  ),
                  OutlinedButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => PageDualList()));
                    },
                    child: Text('듀얼 입장'),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
  }
}
